import { open } from "node:fs/promises";

const PRIORITY = "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

let bagCounter = 0;
let groupCounter = 1;

function intersectionFilter(arr) {
  return (value) => arr.includes(value);
}

async function load() {
  const file = await open(process.cwd() + "/day-3/input");
  const output = [];

  for await (const line of file.readLines()) {
    const l = line.length / 2;

    const a = line.substring(0, l).split("");
    const b = line.substring(l).split("");

    const uniqueA = [...new Set(a)];
    const uniqueB = [...new Set(b)];

    const intersection = uniqueA.filter(intersectionFilter(uniqueB));

    output.push({
      sideA: a,
      sideB: b,
      commonItem: intersection[0],
      group: groupCounter,
    });

    bagCounter++;

    if (bagCounter % 3 === 0) {
      groupCounter++;
    }
  }

  return output;
}

const output = await load();
const prioritySum = output.reduce(
  (sum, backpack) => (sum += PRIORITY.indexOf(backpack.commonItem)),
  0
);

// to find the badge item type, process output in batches of 3 and find the common item
const badgePrioritySum = output.reduce((sum, elf, index) => {
  if (index % 3 !== 0) {
    return sum;
  }

  const elfA = output[index];
  const elfB = output[index + 1];
  const elfC = output[index + 2];

  const uniqueA = [...new Set(elfA.sideA.concat(elfA.sideB))];
  const uniqueB = [...new Set(elfB.sideA.concat(elfB.sideB))];
  const uniqueC = [...new Set(elfC.sideA.concat(elfC.sideB))];

  const A_B = uniqueA.filter(intersectionFilter(uniqueB));
  const A_B_C = A_B.filter(intersectionFilter(uniqueC));

  return (sum += PRIORITY.indexOf(A_B_C[0]));
}, 0);

console.log(`Priority sum (common item):`, prioritySum);
console.log(`Priority sum (badge):`, badgePrioritySum);
