import { open } from "node:fs/promises";

async function loadElfData() {
  const file = await open(process.cwd() + "/day-1/input");
  const output = [];
  let elfIndex = 0;

  for await (const line of file.readLines()) {
    if (!line) {
      elfIndex++;
    } else {
      if (!output[elfIndex]) {
        output[elfIndex] = {
          index: elfIndex,
          total: 0,
          raw: [],
        };
      }

      const value = parseInt(line, 10);

      output[elfIndex].total += value;
      output[elfIndex].raw.push(value);
    }
  }

  return output;
}

const elves = await loadElfData();
elves.sort((a, b) => b.total - a.total);

const topThree = elves.slice(0, 3);

console.log(`Top Elf:`, elves[0].total);
console.log(
  `Top three Elves:`,
  topThree.reduce((total, elf) => (total += elf.total), 0)
);
