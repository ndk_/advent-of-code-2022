import { open } from "node:fs/promises";

/**
 * The winner of the whole tournament is the player with the highest score.
 * Your total score is the sum of your scores for each round. The score for a
 * single round is the score for the shape you selected (1 for Rock, 2 for
 * Paper, and 3 for Scissors) plus the score for the outcome of the round (0
 * if you lost, 3 if the round was a draw, and 6 if you won).
 *
 * Rock: A, X, 1
 * Paper: B, Y, 2
 * Scissors: C, Z, 3
 */

// Rock < Paper < Scissors
const ENEMY_MOVES = ["A", "B", "C"];
const PLAYER_MOVES = ["X", "Y", "Z"];

async function loadStrategyGuide() {
  const file = await open(process.cwd() + "/day-2/input");
  const output = [];

  for await (const line of file.readLines()) {
    const [a, b] = line.split(" ");

    output.push({
      round: `${a} ${b}`,
      scorePt1: getTotalRoundScorePt1(a, b),
      scorePt2: getTotalRoundScorePt2(a, b),
    });
  }

  return output;
}

function getPlayerMoveScore(mv) {
  return PLAYER_MOVES.indexOf(mv) + 1;
}

function getOutcomeScore(a, b) {
  const aIndex = ENEMY_MOVES.indexOf(a);
  const bIndex = PLAYER_MOVES.indexOf(b);
  const winningResponse = getWinningResponse(a);

  switch (true) {
    case b === winningResponse:
      return 6; // win
    case aIndex === bIndex:
      return 3; // draw
    default:
      return 0; // lose
  }
}

function getTotalRoundScorePt1(a, b) {
  return getOutcomeScore(a, b) + getPlayerMoveScore(b);
}

function getWinningResponse(a) {
  const aIndex = ENEMY_MOVES.indexOf(a);
  return PLAYER_MOVES[(aIndex + 1) % PLAYER_MOVES.length];
}

function getDrawingResponse(a) {
  const aIndex = ENEMY_MOVES.indexOf(a);
  return PLAYER_MOVES[aIndex];
}

function getLosingResponse(a) {
  const aIndex = ENEMY_MOVES.indexOf(a);
  return PLAYER_MOVES[aIndex > 0 ? aIndex - 1 : PLAYER_MOVES.length - 1];
}

function getTotalRoundScorePt2(a, b) {
  switch (b) {
    case "Z": // win
      return 6 + getPlayerMoveScore(getWinningResponse(a));
    case "Y": // draw
      return 3 + getPlayerMoveScore(getDrawingResponse(a));
    default: // lose
      return 0 + getPlayerMoveScore(getLosingResponse(a));
  }
}

const output = await loadStrategyGuide();

console.log(
  `Total score (part 1):`,
  output.reduce((total, round) => (total += round.scorePt1), 0)
);

console.log(
  `Total score (part 2):`,
  output.reduce((total, round) => (total += round.scorePt2), 0)
);
